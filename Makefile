CC 		= arm-linux-gnueabi-gcc
CFLAGS 	= -O2 -g -std=gnu99 -W -Wall -Wno-comment
INCLUDES 	= -I./ev3dev-c/source/ev3 -I./headers/
LDFLAGS 	= -L./cross-comp -lrt -lm -lev3dev-c -lpthread -lbluetooth
BUILD_DIR 	= ./build
SOURCE_DIR 	= ./src
BIN_DIR 	= ./bin

OBJS = \
	$(BUILD_DIR)/clamp.o\
	$(BUILD_DIR)/main.o 

.PHONY: all clean docker connect clamp main



all: main 

main:${OBJS}
	$(CC) $(INCLUDES) $(CFLAGS) $(BUILD_DIR)/*.o $(LDFLAGS) -o $@
	mv $@ $(BIN_DIR)

clamp: ${OBJS}
	$(CC) $(INCLUDES) $(CFLAGS) $(BUILD_DIR)/clamp.o $(LDFLAGS) -o $@
	mv $@ $(BIN_DIR)
	
sonar: ${OBJS}
	$(CC) $(INCLUDES) $(CFLAGS) $(BUILD_DIR)/sonar.o $(LDFLAGS) -o $@
	mv $@ $(BIN_DIR)
	
clampRun: ${OBJS}
	$(CC) $(INCLUDES) $(CFLAGS) $(BUILD_DIR)/clampRun.o $(LDFLAGS) -o $@
	mv $@ $(BIN_DIR)

$(OBJS): $(BUILD_DIR)

$(BUILD_DIR):
	mkdir $(BUILD_DIR)

$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.c
	@echo 'xd'
	$(CC) -c $(SOURCE_DIR)/*.c $(INCLUDES) 
	mv *.o $(BUILD_DIR)

clean:
	rm -f $(BUILD_DIR)/*.o
	rm -f $(BIN_DIR)/*
	
docker:
	docker run --rm -it -h ev3 -v ~/Documents/Eurecom/OS/os-project/:/src -w /src ev3dev/debian-jessie-armel-cross /bin/bash

send:
	scp $(BIN_DIR)/* robot@90.92.57.59:~/bin
	
connect:
	ssh robot@90.92.57.59


export LD_LIBRARY_PATH=./ev3dev-c/lib/
