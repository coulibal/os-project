# OS project

run Makefile, executable are in /build.
src contains our code
ev3dev references the original git (reuse their code)
cross-comp for docker
 
# CLAMP UPDATE

clamp.c main contains a switch loop for tests:
To execute the binary clamp inside the clamp folder on the robot follow the instruction >
	./clamp 1 disp		to calibrate the clamp (disp in [-3800,3800]
	./clamp 2 disp		to calibrate the arm (disp in [-70,70]
	./clamp 3 disp 	to calibrate the arm by resetting it first
	./clamp 4 		to reset the clamp
	
Result :

	./clamp 1 disp		disp=-3200 for all to way up to all the way down, disp=-3200 for all the way down to all the way up
	./clamp 2 disp		disp =70 for horizontal to all the way up : the motor might get overloaded for low values of speed (speed ~80 is ok), disp=-66 for all the way up to horizontal
	./clamp 3 disp 	disp=35 for horizontal clamp : there is an offset of -6 hence position=29, disp=99 for all the way up : offset of +1
	./clamp 4
	
Note: 

	The arm motor relative position is nto precise, we should not relly on it. After several ups and downs, its value can shift from 5 points. Whereas the clamp's is reliable.
	
	
Running the armManInit twice back to back yielded different results. The motor position was consistent but the higth of the clamp was not. There was seemingly a difference of power. 
Turned out tweaking the RAMP_UP values was enough.

To avoid stalling the motor, I set the speed at 90. Therefore, I should set a ramp down for such movements.
