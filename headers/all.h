#ifndef IDEMP
#define IDEMP

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>
#include <pthread.h>
#include <sys/time.h>
#include <stdbool.h>

#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"


#include "clamp.h"
#include "config.h"
#include "movements.h"
#include "main.h"

#endif 
