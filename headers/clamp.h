int getArmPosition();

int getClampPosition();

int motorRunning(uint8_t sn );

void runTimed(uint8_t sn, int speed, int time);


void toRelativePosition(uint8_t sn, int speed, int position);

void clampDown(void);
void clampUp(void);
int getClampPosition();

void armRelease();
void armManualInit(void);
int getArmPosition();
void armDownNoReset(int position);
void armRest(int position);
void armUp(int position);
