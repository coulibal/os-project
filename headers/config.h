
#define Sleep( msec ) usleep(( msec ) * 1000 )

//-----------------CLAMP.C

#define T_RAMP_UP 0
#define T_RAMP_DOWN 0
#define RAMP_DOWN_UP 200

#define CLAMP_INIT_SPEED 1350

#define ARM_INIT_SPEED 80
#define ARM_UP_SPEED 110
#define ARM_DOWN_SPEED 20
#define ARM_RELEASE_SPEED 60

#define ARM_DISP_OFFSET -2
#define ARM_INIT_DISP 33
#define ARM_UP_DISP 70
#define ARM_DOWN_DISP -66

#define ARM_RELEASE_DISP 10

#define CLAMP_DOWN_DISP -6000
#define CLAMP_UP_DISP 6000

#define MOTOR_CLAMP	OUTPUT_C
#define MOTOR_ARM	OUTPUT_B

#define DOWN 0
#define REST 1
#define UP 2

//-----------------POSITION.C

//position -> (x:cm, y:cm)   
//map

#define MAP_DIM (119.8,203.4)

//dimension (cube) -> [width:cm, length: cm, length_tot:cm, higth_rest: cm, higth_up: cm, button:cm]
//robot   

#define START_POS (24.1,5.5)
#define ROBOT_DIM [13.1,17.6,33.5,32.8,43.8,1.3]
#define WHEEL_DIAM 0.055
#define WHEEL_RADIUS 0.0275

//dimension (cube) -> (width:cm, depth: cm, higth:cm)
//origin cube

#define OC_POS (84.4,166.2)
#define OC_DIM (15,15,15)

//destination cube

#define DC_POS (19.9, 145.4)
#define DC_DIM (16.1,15.8,15)

//random cubes

//-----------------MAIN.H

#define WIDTH_UNKNWON_AREA 40
#define WIDTH_ARENA 119.8
#define CUBE_DIMENSION 15  

#define LOCATION_FULL_CUBE 166.2
#define LOCATION_EMPTY_CUBE 145.4
#define LOCATION_EMPTY_PYRAMIDE 0

#define UNKNOWN_ZONE 75.8

#define RC_DIM

//----------------------MOVEMENTS.C

#define MOTOR_LEFT	OUTPUT_A
#define MOTOR_RIGHT	OUTPUT_D





