#ifndef MAIN_H
#define MAIN_H

#include <pthread.h> // add -lpthread for compiling 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <math.h>
#include <stdbool.h>

//--------------------- new types def---------------------------

typedef struct Container Container;
Container container_new(float y, bool container, bool empty);

typedef enum Strategy Strategy;

typedef enum Container_type Container_type; 

typedef struct Args_find_next_container Args_find_next_container;
Args_find_next_container new_afnc(int nb_of_half_turn,bool loaded_robot,bool  unknown_cube, Strategy strategy,Container* containers, uint8_t sn[2]);
	
//--------------------- end types def---------------------------


static void container_init(Container* container,int type);
static int type_of_container(float robot_postion_y,int heading,Container container);
static bool container_is_adapted(Container container,Strategy strat,bool loaded_robot);

static void args_find_next_container_init(Args_find_next_container* ars,Container* containers, uint8_t sn_motor[4], uint8_t sn_sensor[3]);
static void* find_next_container(Args_find_next_container* arguments);
static void move_toward_the_wall(uint8_t sn_motor[4], uint8_t sn_sensor[3], int heading);
static void go_to_container(Args_find_next_container* args,Container_type type,int heading);

static void* edge_detection(void* args);
void waiting_for_touch(uint8_t sn_touch); 

static int f_heading(int nb_of_half_turn);
void setElapsedTime(struct timeval t_start, struct timeval t_stop);


#endif
