#define SPEED_LINEAR	25 //linear speed in percentage
#define SPEED_CIRCULAR	5 //circular speed in percentage
#define DEGREE_TO_COUNT(d) ( (d)*260/90 )
#define COUNT_TO_DEGREE(d) ( (d)*90/260 )

int maxSpeed;
uint8_t motor[2];
uint8_t sn_gyro;
uint8_t sn_sonar;
uint8_t sn_touch;
struct timeval getT_start();
int updateYtime(int speed, int time);
int updateYcm(float cm);
int getY();
int getLinSpeed();

void runForever(int rightSpeed, int leftSpeed);
void runToRelativePosition(int rightSpeed,int leftSpeed, int leftPosition, int rightPosition);
void runTimedWheels(int rightSpeed,int leftSpeed, int ms);
int isRunning(void); //return 1 if robot running or 0 if not
int isStalled(void);
void stop(void);

void moveNone();
void moveForward();
void moveForwardTimed(int ms);
void moveBackward();
void moveBackwardTimed(int ms);
void turnLeft();
void turnLeftTimed(int ms);
void turnRight();
void turnRightTimed(int ms);
void turnAngle(int angle);
float getGyroValue();

void threadGyro(int angle);
void gyroTurnLeft();
void gyroTurnRight();

void moveBackwardCm(float cm);
void moveForwardCm(float cm);

void gyroTurnAngle(int angle);

void setMotors(uint8_t sn[2]);
bool initMovements(uint8_t motor_right, uint8_t motor_left);
