
#include "all"

//position[0]:arm, position[1]:clamp
int position[2];

//state flags
char state[64];

int maxSpeed;
uint8_t motor[2];
char address[25];
char saction[25];
size_t sz;

int getArmPosition(){
	return position[0];
}

int getClampPosition(){
	return position[1];
}

int motorRunning(uint8_t sn ){
	int i=0;
	FLAGS_T state = TACHO_STATE__NONE_;
    get_tacho_state_flags( sn, &state );
    if ( state != TACHO_STATE__NONE_ ) i=1;
	printf("motor running stat %d\n",i);
	return i;
}

void runTimed(uint8_t sn, int speed, int time){
//negative down, postitive up 
	set_tacho_speed_sp(sn,speed);
	set_tacho_time_sp(sn,time);
	set_tacho_ramp_up_sp( sn, T_RAMP_UP );
	set_tacho_ramp_down_sp( sn, T_RAMP_DOWN );
	set_tacho_command_inx(sn,TACHO_RUN_TIMED);
}


void toRelativePosition(uint8_t sn, int speed, int position)
//+position up, - position down. Positive speed. For clamp and arm.
{
	set_tacho_stop_action_inx( sn, TACHO_HOLD );
	set_tacho_speed_sp(sn, speed);
	set_tacho_position_sp(sn, position);
	set_tacho_ramp_up_sp( sn, T_RAMP_UP );
	set_tacho_ramp_down_sp( sn, T_RAMP_DOWN );
	set_tacho_command_inx(sn,TACHO_RUN_TO_REL_POS);
}

void armManualInit(void){
	set_tacho_command_inx(motor[0],TACHO_RESET);
   	Sleep(1500);
	set_tacho_stop_action_inx( motor[0], TACHO_HOLD );
	toRelativePosition(motor[0], ARM_INIT_SPEED+10, ARM_INIT_DISP);
}

void armDownNoReset(int position){
	get_tacho_position( motor[0], &position );
	Sleep(100);
	toRelativePosition(motor[0], ARM_INIT_SPEED, -position);
	
}

void armDown(){
	set_tacho_command_inx(motor[0],TACHO_RESET);
	set_tacho_stop_action_inx( motor[0], TACHO_HOLD );
}

void armRest(int position){
	get_tacho_position( motor[0], &position );
	int i=0;
	int ref = ARM_INIT_DISP+ARM_DISP_OFFSET;
	while(abs(position)-ref>4 && i<5){
		set_tacho_stop_action_inx( motor[0], TACHO_HOLD );
		toRelativePosition(motor[0], ARM_INIT_SPEED,ref-position);
		Sleep(400);
		get_tacho_position( motor[0], &position );
		printf(" Position Arm1 : %d, %d  ", position, i);
		i++;
	}
	get_tacho_position( motor[0], &position );
	printf(" Position Arm1 : %d  ", position);
	Sleep(200);
	get_tacho_position( motor[0], &position );
	printf(" Position Arm1 : %d  ", position);
	set_tacho_position( motor[0], 0 );
	
}

void armRelease(){
	toRelativePosition(motor[0], ARM_RELEASE_SPEED, ARM_RELEASE_DISP);
	Sleep(800);
	for(int i=0; i<4; i++){
		toRelativePosition(motor[0], ARM_UP_SPEED, 7);
		Sleep(300);
	}
}

void armUp(int position){
	get_tacho_position( motor[0], &position );
	int disp =0;
	if(position<99){
		disp = ARM_UP_DISP-position - ARM_DISP_OFFSET;
	}
	Sleep(100);
	toRelativePosition(motor[0], ARM_UP_SPEED, disp);
}

void clampDown(void){
	toRelativePosition(motor[1], CLAMP_INIT_SPEED, CLAMP_DOWN_DISP);
}

void clampUp(void){
	toRelativePosition(motor[1], CLAMP_INIT_SPEED, CLAMP_UP_DISP);
}



void clampRest(int position){
	get_tacho_position( motor[1], &position );
	int i=0;
	while(abs(position)>5 && i<5){
		
		toRelativePosition(motor[1], CLAMP_INIT_SPEED, -position);
		Sleep(1000);
		get_tacho_position( motor[1], &position );
		get_tacho_position( motor[1], &position );
		printf(" Position Clamp1 : %d\n", position);
		i++;
		
	}
	get_tacho_position( motor[1], &position );
	printf(" Position Clamp1 : %d\n", position);
	Sleep(200);
	get_tacho_position( motor[1], &position );
	printf(" Position Clamp1 : %d\n", position);
	set_tacho_position( motor[1], 0 );
}

<<<<<<< HEAD
=======

>>>>>>> 0758c4b911ceb23de11cca8a08dba6219f4d009e
