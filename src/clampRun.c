#include "all.h"

int position[2];

//state flags
char state[64];

int maxSpeed;
uint8_t motor[2];
char address[25];
char saction[25];
size_t sz;

int main( int argc, char* argv[])
{

	if(ev3_init() < 1) 
	{
		return 1;
	}
	while( ev3_tacho_init() < 1) 
	{
		printf("sleeping");
		sleep(1000);
	}
	
	for(int i=0; i<argc;i++){
		printf("%s\n",argv[i]);
	}
	
	if ( ev3_search_tacho_plugged_in(MOTOR_ARM,0, &motor[0], 0 ) && ev3_search_tacho_plugged_in(MOTOR_CLAMP,0, &motor[1], 0 )){
	
		
		get_tacho_position( motor[0], &position[0] );
		printf(" Position Arm1 : %d  ", position[0]);
		get_tacho_position( motor[1], &position[0] );
		printf(" Position Clamp1 : %d\n", position[0]);

		const char *argstr = argv[1];
		int arg = atoi(argstr);
		switch (arg)
		{
		case 0:
			clampDown();
			clampRest(position[1]);

			break;

		case 1:
			get_tacho_position( motor[1], &position[1] );
			printf(" Position Clamp0 : %d\n", position[1]);
			int disp = atoi(argv[2]);
			toRelativePosition(motor[1], CLAMP_INIT_SPEED, disp);
			Sleep(2000);
			get_tacho_position( motor[1], &position[1] );
			printf(" Position Clamp1 : %d\n", position[1]);
			break;

		case 2:
			get_tacho_position( motor[0], &position[0] );
			printf(" Position Arm0 : %d  ", position[0]);
			int disp1 = atoi(argv[2]);
			toRelativePosition(motor[0], ARM_INIT_SPEED, disp1);
			get_tacho_state( motor[0], &state, sizeof( state ));
			printf(" State0 : %s  ", state);
			Sleep(1500);
			get_tacho_state( motor[0], state, sizeof( state ));
			printf(" State1 : %s  ", state);
			get_tacho_position( motor[0], &position[0] );
			printf(" Position Arm1 : %d  ", position[0]);
			break;

		case 3:
			get_tacho_position( motor[0], &position[0] );
			printf(" Position Arm0 : %d  ", position[0]);
			int disp2 = atoi(argv[2]);
			set_tacho_command_inx(motor[0],TACHO_RESET);
   			Sleep(1500);
			set_tacho_command_inx(motor[0],TACHO_RESET);
			get_tacho_position( motor[0], &position[0] );
			printf(" Position Arm1 : %d  ", position[0]);
			set_tacho_stop_action_inx( motor[0], TACHO_HOLD );
			toRelativePosition(motor[0], ARM_INIT_SPEED, disp2);
			Sleep(1500);
			get_tacho_position( motor[0], &position[0] );
			printf(" Position Arm2 : %d  ", position[0]);
			break;

		case 4:
			get_tacho_position( motor[1], &position[1] );
			printf(" Position Clamp0 : %d\n", position[1]);
			set_tacho_command_inx(motor[1],TACHO_RESET);
			get_tacho_position( motor[1], &position[1] );
			printf(" Position Clamp1 : %d\n", position[1]);
			break;

		case 5:
			toRelativePosition(motor[0], ARM_RELEASE_SPEED, ARM_RELEASE_DISP);
			Sleep(800);
			for(int i=0; i<3; i++){
				toRelativePosition(motor[0], ARM_UP_SPEED, 3);
				Sleep(300);
			}
			break;

		case 6:

			clampDown();
			Sleep(6000);
			clampRest(position);
			Sleep(5000);
			armRelease();
			Sleep(5000);
			armManualInit();
			break;
		
		default:
			printf("in default");
			break;
		}

   	}
	
	ev3_uninit();
	
	return 0;
}