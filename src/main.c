#include "all.h"
#include "main.h"
#include "clamp.h"
#include "movements.h"

/*
*****************************
Coded by Jean-Baptiste Peyrat
*****************************
*/

// ---------------------- Global variables ---------------------------------

// initialize the flag 
bool edge_detected = false; 

int usec=0;
float robot_y_pos;


//------------------- New types definition ------------------------------

// motors and sensors: 


typedef enum Motor
{
    M_RIGHT, M_LEFT, M_CLAMP, M_ARM
} Motor; 


typedef enum Sensor
{
    TOUCH, SONAR, GYRO
} Sensor; 

typedef struct  Container {
    float location_y;
	bool container_for_balls; // container where we have to put the balls 
	bool empty; // true if the container is empty 
}Container;

Container container_new(float y, bool container, bool empty){
	Container c;
	c.location_y=y;
	c.container_for_balls=container;
	c.empty=empty;
	return c;
};

typedef enum Container_type
{
	FULL_CUBE, EMPTY_CUBE, PYRAMIDE, UNKNOWN_CUBE

} Container_type;


typedef enum Strategy
{
    CUBE_ONLY , CUBE_AND_PYRAMID
} Strategy;  


typedef struct Args_find_next_container 
{
	int nb_of_half_turn;
	bool loaded_robot;
	bool  unknown_cube;
	Strategy strategy; 
	Container* containers;
	uint8_t sn_motor[4];
	uint8_t sn_sensor[3];
  
} Args_find_next_container;

// ------------------------ initialization of the suctures ------------------------------

/*
initilize a container 
*/
static void container_init(Container* container,int type)
{
	switch(type)
	{
	case 0:
		container->location_y = LOCATION_FULL_CUBE;
		container->container_for_balls = false;
		container->empty = false; 
		break;
	case 1:
		container->location_y = LOCATION_EMPTY_CUBE;
		container->container_for_balls = true; 
		container->empty = true;
		break;
	case 2:
		container->location_y = LOCATION_EMPTY_PYRAMIDE;
		container->container_for_balls = true;
		container->empty = true; 
		break;
	case 3:
		container->location_y = 0; 
		container->container_for_balls = false; 
		container->empty = false; 
		break;
	}
}

static void args_find_next_container_init(Args_find_next_container* args,Container* containers, uint8_t sn_motor[4], uint8_t sn_sensor[3])
{
	// initial parameters: 
	args->nb_of_half_turn = 0; 
	args->loaded_robot = false; // set to one when the robot holds balls
	args->unknown_cube = false; // set to one when the unknown cube is spotted
	args->strategy = CUBE_ONLY;
	
	args->containers = containers;
	args->sn_motor[0]=sn_motor[0];
	args->sn_motor[1]=sn_motor[1];
	//args->sn_motor[2]=sn_motor[2];
	//args->sn_motor[3]=sn_motor[3];
	
	args->sn_sensor[0]=sn_sensor[0];
	args->sn_sensor[1]=sn_sensor[1];
	args->sn_sensor[2]=sn_sensor[2];

}


//---------------------- Annex functions -------------------------------------------

/* Coded by Nicolas */
void setElapsedTime(struct timeval t_start, struct timeval t_stop)
{
	gettimeofday(&t_stop, NULL);
	usec = (int)(t_stop.tv_usec - t_start.tv_usec)+1000000*(t_stop.tv_sec - t_start.tv_sec);
}

/* 
return -1 if the robot heads the north or 1 if it heads toward the south
*/
static int f_heading(int nb_of_half_turn)
{
	int result = 1;
	if (nb_of_half_turn%2==1)
	{
		result = -1;
	}
	return result; 
}

// ----------------------- MAIN functions of the strategy ------------------------------

/*
Go to container:
If the container is the unknown cube:
- push it to the wall and position the robot back to detect this cube again
Else:
- stop when we touch it and call grab or release (depends on the type of container) 
*/
static void go_to_container(Args_find_next_container* args,Container_type type,int heading)
{
	printf("in go_to_container \n");
	
	// put the robot in the right positon for going forward only (depends on the heading of the robot !!! (empirical approach)  
	moveForwardCm(8);
	/* while( motorRunning((args->sn_motor)[M_LEFT]) && motorRunning((args->sn_motor)[M_RIGHT]) ){
		continue;
	}; */
	sleep(3);
	printf("in go_to_container: the motors are stopped\n");
	gyroTurnAngle(heading*90); 
	sleep(2);
	
	// Position the robot in front of the container
	moveForward();
	waiting_for_touch((args->sn_sensor)[TOUCH]);
	moveNone();
	
	// Grab or release the balls 
	if ((args->containers)[type].container_for_balls)
	{
		printf("in go_to_container: Release the balls\n");
		sleep(5);
		
		armRelease();
		sleep(5);
		armManualInit();
		sleep(2);

		args->loaded_robot = false;
		(args->containers)[type].empty = false;
	}
	else 
	{
		printf("in go_to_container: Grab the balls\n");
		sleep(5);
		
		clampDown();
		sleep(5);
		clampUp();
		sleep(5);

		args->loaded_robot = true;
		(args->containers)[type].empty = true;
	}	 
}

/*
1) The robot move forward until it finds the edge of a container or the end of the map
2) If it finds the edge of the container:
- first it determines which container it is
- then it takes an action accordingly
3) Go back to the first step
 */
static void* find_next_container(Args_find_next_container *args)
{
<<<<<<< HEAD
	Args_find_next_container *args = arguments; // cast to the proper type
	int type; 
=======
	int type =0; 
>>>>>>> 0758c4b911ceb23de11cca8a08dba6219f4d009e
	int heading = 0; 
	
	printf("in find_next_container \n");

	while(1) 
	{
		struct timeval t_stop;
		moveForward();
<<<<<<< HEAD
		sleep(1);
=======
		printf("in find_next_container past moveForward\n");
>>>>>>> 0758c4b911ceb23de11cca8a08dba6219f4d009e

		while(!(edge_detected))
		{
			continue;
		}; // loop while there is no edge detected 
		printf("in find_next : edge_detected\n");
		
		// coded by Nicolas: Start
		gettimeofday(&t_stop, NULL);
		setElapsedTime(getT_start(),t_stop);
		robot_y_pos = getY() + updateYtime(getLinSpeed(), abs(usec/1000));
<<<<<<< HEAD

		printf ("robotPos : %f, time: %f\n", robot_y_pos, usec);
			
		stop();
		sleep(1);
=======
		// End 

		moveNone();
		sleep(2);
>>>>>>> 0758c4b911ceb23de11cca8a08dba6219f4d009e
		heading = f_heading(args->nb_of_half_turn);

		type = type_of_container(robot_y_pos,heading,(args->containers)[EMPTY_CUBE]);
		if(container_is_adapted((args->containers)[type],args->strategy,args->loaded_robot)) 
		{
			// If the robot deals with the unknown cube for the first time
			if((type == UNKNOWN_CUBE) && args->unknown_cube == false) 
			{
				move_toward_the_wall(args->sn_motor,args->sn_sensor,heading);
				sleep(1);
				moveBackwardTimed(1000);  //need to be tuned
				sleep(1);
				gyroTurnAngle(-heading*90); 
				sleep(1);
				moveBackwardTimed(1000);  //need to be tuned
				args->unknown_cube = true;
			}
			else  
			{
				go_to_container(args,type,heading); 
				// Position the robot back in the right position
				moveBackwardTimed(1000);
				sleep(1);  

				// Choose the right direction to go 
				bool cond1 = (type == FULL_CUBE) && (heading == 1); // origin cube on the right side of the arena 
				bool cond2 = (type == UNKNOWN_CUBE) && (heading == -1); // unknown cube in the left part of the arena

				if(cond1||cond2) // need to head in the opposite direction
				{
					printf("Go in the opposite direction\n");
					gyroTurnAngle(heading*90);
					(args->nb_of_half_turn) += 1; // since it changes its direction the heading changes too  			
				}  
				else // continue in the same direction as previously 
				{
					printf("Same heading\n");
					gyroTurnAngle(-heading*90);
				}
			}
		}
		else 
		{
			printf("container is not adapted\n");
		}
	sleep(2); // let the time to the sonar to initialize again  
	edge_detected = false;  	
	}	 
}

/*
move the cube toward the wall and then go back to be able to detect it again 
(find_next_container and then go_to_container)
*/
static void move_toward_the_wall(uint8_t sn[4],uint8_t sn_sensor[3],int heading) 
{

	printf("in move_toward_the_wall \n");

	// put the robot in the right positon for pushing the cube 
	moveForwardCm(8);
	/* while(motorRunning(sn[M_RIGHT]) && motorRunning(sn[M_LEFT])){
		continue;
	};  */
	sleep(4);
	printf("in move_toward_the_wall: after while");
	gyroTurnAngle(heading*90);

	// Push the cube toward the wall and stop 	
	moveForward();
	waiting_for_touch(sn_sensor[TOUCH]);
	moveNone();
}

/*
Choose between different strategy to known wether or not we want to go to a specific container 
*/
static bool container_is_adapted(Container container,Strategy strat,bool loaded_robot)
{ 
	printf("in container_is_adapted\n");

	bool verdict = false; 
	switch(strat)
	{
	case 0:
		if(!loaded_robot && !container.container_for_balls || loaded_robot && container.container_for_balls)
		{
			verdict = true;
		}
		break;
	
	case 1:
		if(!loaded_robot && container.empty || loaded_robot && !container.empty)
		{
			verdict = true;
		}
		break;
	
	}

	printf("in container_is_adapted: verdict = %d \n",verdict);
	return verdict; 
}	


/*
 Just look at the different cube and find the one that correponds to the one we spot with the sonar 
 Return: an int! 
 */
int type_of_container(float robot_position_y,int heading,Container container)
{
	printf("in type_of_container\n");

	int type = -1;
	printf("robot postion = %f ; UNKNOWN_ZONE = %f \n",robot_y_pos,UNKNOWN_ZONE);
	if (robot_y_pos < UNKNOWN_ZONE)
	{
		type = UNKNOWN_CUBE;
	}
	else if (heading == 1)
	{
		type = FULL_CUBE;
	}
	else
	{
		if (container.location_y < robot_position_y )
		{
			type = EMPTY_CUBE;
		}
	}
	type = FULL_CUBE;
	printf("in type_of_container:The container type is %d \n",type);
	return type;
}


/*
set the global variable edge_detected to true when a container is spotted by the sonar sensor
*/
static void* edge_detection(void* arguments) 
{
	printf("in edge_detection \n");

<<<<<<< HEAD
	if(ev3_search_sensor(LEGO_EV3_US, &(sn_sensor[SONAR]),0)){
=======
	Args_find_next_container *args = arguments;
	
	// initialize the sonar sensor
	if(ev3_search_sensor(LEGO_EV3_US, &((args->sn_sensor)[SONAR]),0)){
>>>>>>> 0758c4b911ceb23de11cca8a08dba6219f4d009e
		int portSensor = 0;
		char portNameSensor[256];
		char mode[256];

		printf("sonar initialized \n");
<<<<<<< HEAD
		portSensor = ev3_sensor_desc_port(sn_sensor[SONAR]);
		ev3_sensor_port_name(sn_sensor[SONAR],portNameSensor);
		get_sensor_mode(sn_sensor[SONAR],mode,sizeof(mode));

		printf("\tPort = %d %s \n",portSensor,portNameSensor);
		printf("\tmode = %s\n", mode); 
	}else{
		printf("sonar is unplugged \n");
	}

	Args_detection_function *args = arguments; // cast to the proper type 
=======
		portSensor = ev3_sensor_desc_port((args->sn_sensor)[SONAR]);
		ev3_sensor_port_name((args->sn_sensor)[SONAR],portNameSensor);
		get_sensor_mode((args->sn_sensor)[SONAR],mode,sizeof(mode));
		printf("\tPort = %d %s \n",portSensor,portNameSensor);
		printf("\tmode = %s\n", mode); 
		
	}
	else{ printf("sonar is unplugged \n");}
	
	// edge detection
>>>>>>> 0758c4b911ceb23de11cca8a08dba6219f4d009e
	float value = 0;
	float prev_value = INFINITY;
	float distance = 0; 
	
	// Constantly scan the environment: 
	while(1)
	{
<<<<<<< HEAD
		printf("in egde_detection: value=%f \n",value);
		//printf("in egde_detection prev_value=%f \n",prev_value);

		if ( !get_sensor_value0(sn_sensor[SONAR], &value)) 
=======
		// Get the value from the sensor 
		if ( !get_sensor_value0((args->sn_sensor)[SONAR], &value)) 
>>>>>>> 0758c4b911ceb23de11cca8a08dba6219f4d009e
		{
			printf("value sonar error\n");
			value = 0;
		}	
		//printf("value=%f \n",value);
		fflush( stdout );
		// If the value changes by more than CUBE_DIMENSION-2 cm between two consecutive measures then it encounters a container 
		distance = value-prev_value; 
		if (distance >= (CUBE_DIMENSION-2)) // accuracy +/- 1 cm 
		{
			edge_detected = true; 
		}
		prev_value = value;
	}
}

void waiting_for_touch(uint8_t sn_touch)
{
	printf("in waiting_for_touch \n");
	float value; 
	get_sensor_value0(sn_touch,&value);
	while (value==0)
	{
		get_sensor_value0(sn_touch,&value); 
	}
	printf("in waiting_for_touch: sensor touched \n");
}


<<<<<<< HEAD
static void args_detection_function_init(Args_detection_function* args,uint8_t sn,bool* flag)
{
	//args = (Args_detection_function*)malloc(sizeof(Args_detection_function));
	args->sn = sn;
	args->flag = flag; 
}

static void args_find_next_container_init(Args_find_next_container* args,bool* obstacle_touched, bool* edge_detected, bool* loaded_robot, Strategy strategy,Container* containers, uint8_t sn[2])
{
	//args = (Args_find_next_container*)malloc(sizeof(Args_find_next_container));
	args->obstacle_touched = obstacle_touched;
	args->edge_detected = edge_detected; 
	args->nb_of_half_turn = 0; // heading = nb_of_half_turn % 2 = (North if 1 || South if 0)
	args->loaded_robot = loaded_robot; 	
	args->unknown_cube = false; // set to one when the unknown cube is spotted
	args->strategy = strategy;
	args->containers = containers;
	args->sn[0]=sn[0];
	args->sn[1]=sn[1];

}

=======
>>>>>>> 0758c4b911ceb23de11cca8a08dba6219f4d009e
/*
*
***********************************
*	MAIN
***********************************
*
*/

int main(int argc, char* argv[])
{
	// initialize the different containers 
	int i = 0;
	Container containers[4];
	for(i=0;i<4;i++) container_init(&containers[i],i);

	uint8_t sn_motor[4];
	uint8_t sn_sensor[3];


	// initialize the robot
	ev3_init();

	//initialize the sensors (apart form the sonar) 
	while( ev3_sensor_init() < 1) 
	{
		printf("Sensors could not be init\n");
		sleep(1000);
	}

	if(ev3_search_sensor(LEGO_EV3_TOUCH, &(sn_sensor[TOUCH]),0))
	{
		printf("touch initialized \n");
	}
	else{ printf("touch is unplugged \n");}

	if(ev3_search_sensor(LEGO_EV3_GYRO, &(sn_sensor[GYRO]),0))
	{
		printf("gyro initialized \n");
	}
	else{printf("gyro is unplugged \n");}

<<<<<<< HEAD
	
=======
	if(ev3_search_sensor(LEGO_EV3_US, &(sn_sensor[SONAR]),0))
	{
		printf("sonar initialized \n");
	}
	else{ printf("sonar is unplugged \n");}

>>>>>>> 0758c4b911ceb23de11cca8a08dba6219f4d009e
	
	//initialize the motors
	while( ev3_tacho_init() < 1) 
	{
		printf("Motors could not be init\n");
		sleep(1000);
	}

<<<<<<< HEAD
	if(ev3_search_tacho_plugged_in(MOTOR_RIGHT,0, &sn_motor[M_RIGHT],0) && ev3_search_tacho_plugged_in(MOTOR_LEFT,0, &sn_motor[M_LEFT],0)){

		printf("All motors initialized  :)\n");
       
	    initMovements(sn_motor[M_RIGHT],sn_motor[M_LEFT]);
	}else{
		printf("A motor is unplugged \n");
=======
	if(ev3_search_tacho_plugged_in(MOTOR_RIGHT,0, &sn_motor[M_RIGHT],0) && ev3_search_tacho_plugged_in(MOTOR_LEFT, 0, &sn_motor[M_LEFT],0))
	{
		printf("All motors initialized  :)\n"); 
		initMovements(sn_motor[M_RIGHT],sn_motor[M_LEFT]);
>>>>>>> 0758c4b911ceb23de11cca8a08dba6219f4d009e
	}
	else{printf("A motor is unplugged \n");}

	// main function
	Args_find_next_container find_next_container_args;
    args_find_next_container_init(&find_next_container_args,containers, sn_motor, sn_sensor);  

	// Create the threads and assign them to their tasks
	pthread_t sonar_thread; 
	int err = pthread_create(&sonar_thread, NULL,edge_detection,(void *) &find_next_container_args);
	if (err)
    {
        printf("An error occured with the thread: %d", err);
        return 1;
<<<<<<< HEAD
    }else{
		printf("All threads OK\n");
	} 
    

    
   	pthread_join(sonar_thread, NULL);
	pthread_join(touch_thread,NULL);
	pthread_join(main_thread,NULL);

	float value,prev_value;

	while(1)
	{
		printf("in egde_detection: value=%f \n",value);
		//printf("in egde_detection prev_value=%f \n",prev_value);

		if ( !get_sensor_value0(sn_sensor[SONAR], &value)) 
		{
			value = 0;
			printf("value sonar error\n");
		}	
		// If the value changes by more than CUBE_DIMENSION-2 cm between two consecutive measures then it encounters a container 
		if ((value-prev_value) >= (CUBE_DIMENSION-2)) // accuracy +/- 1 cm 
		{
			printf("in egde_detection: edge detected, value= %f; prev_value = %f \n",value,prev_value);
		}
		prev_value = value;

	}


=======
    }
    else{printf("thread OK\n");} 

	find_next_container(&find_next_container_args);
   	pthread_join(sonar_thread, NULL);
>>>>>>> 0758c4b911ceb23de11cca8a08dba6219f4d009e
    
    return 0;
}


