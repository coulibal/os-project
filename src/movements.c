#include "all.h"
#include "../headers/movements.h"
#include "../headers/config.h"
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <math.h>
#include "ev3.h"
#include "ev3_tacho.h"
#include "ev3_port.h"
#include "ev3_sensor.h"
#include "pthread.h"
#define MOTOR_LEFT	OUTPUT_A
#define MOTOR_RIGHT	OUTPUT_D




//  defining the overall speed to use for the motor
/*#define SPEED_LINEAR	25 //linear speed in percentage
#define SPEED_CIRCULAR	5 //circular speed in percentage
#define DEGREE_TO_COUNT(d) ( (d)*260/90 )
#define COUNT_TO_DEGREE(d) ( (d)*90/260 )

int maxSpeed;
uint8_t motor[2];*/

int speedLinear, speedCircular;
int waitStopped;
int angle;

float y=0;
struct timeval t_start;

int app_alive;

int updateYcm(float cm){
	float angle = getGyroValue();
	float angleDecimal = angle - (float)(abs(angle));
	int angleMod = abs(angle)%360;
	float y_projection = cos((float)angleMod+angleDecimal)*cm;
	return y_projection;
}

int updateYtime(int speed, int time){
	int taco_count = speed*time;
	float cm =taco_count*( (WHEEL_RADIUS*100) + (WHEEL_DIAM*100)+ 14.75)/436.36;
	return updateYcm(cm);
}

struct timeval getT_start(){
	return t_start;
}

int getY(){
	return y;
}

int getLinSpeed(){
	return  maxSpeed * SPEED_LINEAR/100; 
}

void setMotors(uint8_t sn[2]){
	printf("in run setMotors\n");

	motor[0]=sn[0];
	motor[1]=sn[1];
}

void runForever(int rightSpeed, int leftSpeed)
{
	printf("in run Forever\n");
	gettimeofday(&t_start, NULL);
	multi_set_tacho_ramp_up_sp(motor,10);// start the robot smoothly so that the robot don't drop the ball 
	set_tacho_speed_sp(motor[0],rightSpeed);
	set_tacho_speed_sp(motor[1],leftSpeed);
	multi_set_tacho_command_inx(motor,TACHO_RUN_FOREVER);
	
}

void runToRelativePosition(int rightSpeed,int leftSpeed, int leftPosition, int rightPosition)
{
	if(rightSpeed==leftSpeed && leftPosition==rightPosition){
		float cm =leftPosition*( (WHEEL_RADIUS*100) + (WHEEL_DIAM*100)+ 14.75)/436.36;
		y+=updateYcm(cm);
	}
	multi_set_tacho_ramp_up_sp(motor,0);
	set_tacho_speed_sp(motor[0], rightSpeed);
	set_tacho_speed_sp(motor[1], leftSpeed);
	set_tacho_position_sp(motor[0], rightPosition);
	set_tacho_position_sp(motor[1], leftPosition);
	multi_set_tacho_command_inx(motor,TACHO_RUN_TO_REL_POS);
	
}
void runToAbsolutePosition(int rightSpeed, int leftSpeed, int leftPosition, int rightPosition)
{
	multi_set_tacho_ramp_up_sp(motor,0);
	set_tacho_speed_sp(motor[0], rightSpeed);
	set_tacho_speed_sp(motor[1], leftSpeed);
	set_tacho_position_sp(motor[0], rightPosition);
	set_tacho_position_sp(motor[1], leftPosition);
	multi_set_tacho_command_inx(motor,TACHO_RUN_TO_ABS_POS);
	
}
void runTimedWheels(int rightSpeed,int leftSpeed, int ms)
{
	multi_set_tacho_ramp_up_sp(motor,0);
	set_tacho_speed_sp(motor[0], rightSpeed);
	set_tacho_speed_sp(motor[1], leftSpeed);
	multi_set_tacho_time_sp(motor,ms);
	multi_set_tacho_command_inx(motor,TACHO_RUN_TIMED);
	
}


int isRunning(void) // return 1 if one of the motor is running
{
//this function is able to get the motors state if we are not using TACHO_RUN_TO_REL_POS command
	FLAGS_T state = TACHO_STATE__NONE_;
	get_tacho_state_flags(motor[0], &state);
	if(state != TACHO_STATE__NONE_) return ( 1 );
	get_tacho_state_flags(motor[1], &state);
	if(state != TACHO_STATE__NONE_) return ( 1 );
	return ( 0 );
}

int isStalled(void) 
{
	FLAGS_T state= TACHO_STATE__NONE_;
	get_tacho_state_flags(motor[0], &state);
	if(state == TACHO_STALLED)  return ( 1 );
	get_tacho_state_flags(motor[1], &state);
	if(state == TACHO_STALLED)  return ( 1 );
	return (0);
}

void stop(void)
{
	set_tacho_stop_action_inx( motor[0], TACHO_COAST );
	set_tacho_command_inx(motor[0],TACHO_RESET);
}


void moveNone()
{
	printf("Coasting");
	stop();
}

void moveForward()
{
	printf("in moveForward\n");
	speedLinear = maxSpeed * SPEED_LINEAR/100;	
	runForever(-speedLinear,-speedLinear);
}
void goRelativeAB(int x, int y)
{
	speedLinear = maxSpeed * SPEED_LINEAR/100;
	runToRelativePosition(speedLinear,speedLinear, x, y);
}
void goAbsoluteAB(int x,int y)
{	
	speedLinear = maxSpeed * SPEED_LINEAR/100;
	runToAbsolutePosition(speedLinear,speedLinear, x, y);
}
void moveForwardCm(float cm)
{
	int i=0,modulus=0,n=0;
	float constant=10;
	speedLinear = maxSpeed * SPEED_LINEAR/100;


	if(cm <= constant)
	{
		float deg = (436.36*cm)/( (WHEEL_RADIUS*100) + (WHEEL_DIAM*100)+ 14.75); 
        	runToRelativePosition(speedLinear,speedLinear, -deg, -deg);
	}	
	else
	{
		modulus = fmodf(cm,constant);
		n = (int)(cm/constant);
		for(i=0; i < n; i++)
		{	
		float deg = (436.36*constant )/( (WHEEL_RADIUS*100) + (WHEEL_DIAM*100)+ 14.75); 
		 	runToRelativePosition(speedLinear,speedLinear, -deg, -deg);
		}
		if(modulus!=0)
		{
			float deg = (436.36*modulus)/( (WHEEL_RADIUS*100) + (WHEEL_DIAM*100)+ 14.75); 
		 	runToRelativePosition(speedLinear,speedLinear, -deg, -deg);
		}
	}
}
void moveBackwardCm(float cm)
{
	int i=0,modulus=0,n=0;
	float constant=10;
	speedLinear = maxSpeed * SPEED_LINEAR/100;
	if(cm <= constant)
	{
		float deg = (436.36*cm)/( (WHEEL_RADIUS*100) + (WHEEL_DIAM*100)+ 14.75); 
        	runToRelativePosition(speedLinear,speedLinear, deg, deg);
	}	
	else
	{
		modulus = fmodf(cm,constant);
		n = (int)(cm/constant);
		for(i=0; i < n; i++)
		{	
		float deg = (436.36*constant )/( (WHEEL_RADIUS*100) + (WHEEL_DIAM*100)+ 14.75); 
		 	runToRelativePosition(speedLinear,speedLinear, deg, deg);
		}
		if(modulus!=0)
		{
			float deg = (436.36*modulus)/( (WHEEL_RADIUS*100) + (WHEEL_DIAM*100)+ 14.75); 
		 	runToRelativePosition(speedLinear,speedLinear, deg, deg);
		}
	}



}
void moveBackward()
{	
	speedLinear = maxSpeed * SPEED_LINEAR/100;	
	runForever(speedLinear,speedLinear);
}

void turnLeft()
{	
	speedCircular= maxSpeed * SPEED_CIRCULAR/100;
	runForever(-speedCircular,speedCircular);
}

void turnLeftTimed(int ms )
{
	speedCircular=maxSpeed * SPEED_CIRCULAR/100;
	runTimedWheels(-speedCircular,speedCircular,ms);
}
void turnRight()
{	
	speedCircular= maxSpeed * SPEED_CIRCULAR/100;
	runForever(speedCircular,-speedCircular);
}
void turnRightTimed(int ms)
{
	speedCircular=maxSpeed * SPEED_CIRCULAR/100;
	runTimedWheels(speedCircular,-speedCircular,ms);
}

void turnAngle(int angle) 
{
  speedCircular= maxSpeed * SPEED_CIRCULAR/100;
  if(angle > 0) 
  {
  	runForever(speedCircular, -speedCircular);
  }
  else if (angle < 0)
  {
	runForever(-speedCircular, speedCircular);
  }
    
}

void moveBackwardTimed(int ms)
{
	speedLinear=maxSpeed * SPEED_LINEAR/100;
	runTimedWheels(speedLinear,speedLinear, ms);
}

void moveForwardTimed(int ms)
{
	speedLinear=maxSpeed * SPEED_LINEAR/100;

	printf("start moving speed is: %d\n",speedLinear);
	runTimedWheels(-speedLinear,- speedLinear, ms);
}
float  getGyroValue()
{
	float value;
 	if(ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro, 0))
	{
		get_sensor_value0(sn_gyro,&value);
	}
	return value;	
}


void threadGyro(int angle)
{

 	float beforeMovement , duringMovement, totalDegree;

	float floatAngle=fabsf((float)angle);
	beforeMovement= getGyroValue();

	//printf("before Movement: %f\n",beforeMovement);
	do
	{
		duringMovement= getGyroValue();

		//printf("the sonar value: %f\n",getSonarValue());
		//printf("touch value: %f\n",getTouchValue());
	    //printf("during movement: %f\n",duringMovement);
		totalDegree = fabsf(beforeMovement - duringMovement);
		if( floatAngle > 10)
		{
			if( (totalDegree + 10) >=  floatAngle ) //trying to avoid sensors error
			{
				stop();
			}
		}
		else
		{
		  if(totalDegree >=  floatAngle )
			{
				stop();	
			}
		}

		//printf("totalDegree :%f\n",totalDegree);
	}while( isRunning() == 1 );
}
void checkIfStalled()
{
	do{
		FLAGS_T state1= TACHO_STATE__NONE_;
		get_tacho_state_flags(motor[0], &state1);
		if(state1==TACHO_STATE__NONE_) { printf("\n no state ...");}
		if(state1==TACHO_RUNNING) { printf("\n running ...");}
		if(state1==TACHO_RAMPING) { printf("\n ramping ...");}
		if(state1==TACHO_HOLDING) { printf("\n holding ...");}
		if(state1==TACHO_OVERLOADED) { printf("\n overloaded ...");}
		if(state1==TACHO_STALLED) { printf("\n stalled ...");}
	}while(isStalled()==0);
	printf("motors stalled\n");
	moveNone();
}

void gyroTurnAngle(int angle)
{
	float totalAngle=0;
	float floatAngle=(float)angle;
	if(floatAngle>0){
		angle+=7;
	}else{
		angle-=7;
	}
	float beforeMovement, afterMovement;
	pthread_t thread_id[2];
	beforeMovement = getGyroValue();
	pthread_create(&thread_id[0],NULL,(void*)turnAngle,(void*)angle);
	Sleep(100);
	pthread_create(&thread_id[1],NULL,(void*)threadGyro,(void*)angle);
	pthread_join(thread_id[0],NULL);
	pthread_join(thread_id[1],NULL);
	afterMovement=getGyroValue();	
	totalAngle=beforeMovement-afterMovement;
	printf("\ntotal Angle Commited: %f \n", totalAngle);
}

void gyroTurnLeft()
{
	gyroTurnAngle(-180);
}

void gyroTurnRight()
{
 	 gyroTurnAngle(180);
}

bool initMovements(uint8_t motor_right,uint8_t motor_left)
{
	bool bitRight = false;
	bool bitLeft = false;

	motor[0]=motor_right;
	motor[1]=motor_left;
	printf("initializing ...\n");
	if(ev3_search_tacho_plugged_in(MOTOR_RIGHT,0, &motor[0],0)) //motor[0] is the right motor
	{
		set_tacho_command_inx(motor[0], TACHO_RESET);
		get_tacho_max_speed(motor[0], &maxSpeed);
		printf("right motor up\n");
		bitRight = true;
	}

	if(ev3_search_tacho_plugged_in(MOTOR_LEFT,0, &motor[1],0))
	{
		set_tacho_command_inx(motor[1], TACHO_RESET);
		get_tacho_max_speed(motor[1], &maxSpeed);
		printf("left motor up\n");
		bitRight = true;

	}

	return bitLeft&&bitRight;
	
}

/* int main(int argc, char **argv)
{
	int choice=0;
	int port=0;
	int cmpt=0;
	int rate=0;
	int ms=0;
	int angle;
	int val;
	int x=0,y=0;
	float cm=0;
	int test_sensor = 0;
	pthread_t thread_id[2],thread_touch;
	uint32_t n,i,ii; 
	char s[256];
	if(ev3_init() < 1) 
	{
		return (1);
	}
	while( ev3_tacho_init() < 1) 
	{
		Sleep(1000);
	}
	printf(" dd: %d",ev3_sensor_init());
	//initializing the motor 
	//port b and port d
	if(ev3_search_tacho_plugged_in(MOTOR_RIGHT,0, &motor[0],0)) //motor[0] is the right motor
	{
		set_tacho_command_inx(motor[0], TACHO_RESET);
		get_tacho_max_speed(motor[0], &maxSpeed);
		printf("right motor up\n");
	}

	if(ev3_search_tacho_plugged_in(MOTOR_LEFT,0, &motor[1],0))
	{
		set_tacho_command_inx(motor[1], TACHO_RESET);
		get_tacho_max_speed(motor[1], &maxSpeed);
		printf("left motor up\n");
	}

	if(argc <=1)
	{
		printf("0.Exit\n1.Rotation using gyro\n2.Move Forward Timed\n3.Check robot state flag if stuck\n4.check if robot got a ball in hands\n5.stop robot\n6.Move Backward Timed\n7.Turn Left Timed\n8.Turn Right Timed\n9.Turn to the left using gyro \n10.Turn to the right using gyro\n11.check if all sensors plug in\n12.go relative position\n13.go absolute position\n14.Move Forward centimeter\n15.Move Backward centimeter\n16. move forward thread sonar sensor\n");
	}
	else
	{
		choice=atoi(argv[1]);
		switch(choice)
		{
			case 1:
				printf("provide angle: ");
				scanf("%d",&angle);
				gyroTurnAngle(angle);
				break;

			case 2:	
				printf("provide ms: ");
				scanf("%d", &ms);
				moveForwardTimed(ms);
				break;
			case 3:
				//pthread_t thread_id[2];
				pthread_create(&thread_id[0],NULL,&moveBackwardTimed,10000);
				pthread_create(&thread_id[1],NULL,&checkIfStalled,NULL);
				pthread_join(thread_id[0],NULL);
				pthread_join(thread_id[1],NULL);	
				break;
			case 4:

				break;
			case 5:
				stop();
				break;
			case 6:
				printf("provide ms: ");
				scanf("%d", &ms);
				moveBackwardTimed(ms);
				break;
			case 7:
				printf("provide ms: ");
				scanf("%d", &ms);
				turnLeftTimed(ms);
				break;
			case 8:
				printf("provide ms: ");
				scanf("%d", &ms);
				turnRightTimed(ms);
				break;
			case 9:
				gyroTurnLeft();
				break;
			case 10:
				gyroTurnRight();
				break;
			case 11:
				for(test_sensor=0; test_sensor < DESC_LIMIT; test_sensor++)
				{
					if(ev3_tacho[test_sensor].type_inx != TACHO_TYPE__NONE_)
					{
						printf(" type= %s\n", ev3_tacho_type(ev3_tacho[test_sensor].type_inx));
						printf(" port= %s\n", ev3_tacho_port_name(test_sensor,s));
						printf(" port = %d\n", ev3_tacho_desc_port(test_sensor), ev3_tacho_desc_extport(i));
					}
				}
				if ( ev3_search_sensor( LEGO_EV3_TOUCH, &sn_touch, 0 )) 
				{
					 printf( "TOUCH sensor is found\n" );
				}
			       if (ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0))
			        {
					printf(" SONAR sensor is found\n" );
			        }	       
				if(ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro,0))
				{
					printf(" GYRO sensor if found\n" );
				}
				break;
			case 12:
                                printf("provide x: ");
				scanf("%d", &x);
                                printf("provide y: ");
				scanf("%d", &y);
				goRelativeAB( x, y);
				break;
			case 13:
                                printf("provide x: ");
				scanf("%d",&x);
				printf("provide y: ");
				scanf("%d", &y);
				goAbsoluteAB(x,y);
				break;
			case 14:
				printf("provide cm: ");
				scanf("%f",&cm);
				moveForwardCm(cm);
				break;
			case 15:
				printf("provide cm: ");
				scanf("%f", &cm);
				moveBackwardCm(cm);
				break;
			case 16:
				printf("provide cm: ");
				scanf("%f", &cm);
				threadCubeForward(cm);
				break;
				
			default:
				break;

		}
	}
	ev3_uninit();
	return 0;
}

 */
