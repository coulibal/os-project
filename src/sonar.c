/*

Constant speed v
Precision: 1 point every x cm
distance = l = 75cm

nb of points = (1/x)*l
Measure every = x/v

*/	
#include "all.h"
#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"

// WIN32 /////////////////////////////////////////
#ifdef __WIN32__

#include <windows.h>

// UNIX //////////////////////////////////////////
#else

#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )

//////////////////////////////////////////////////
#endif

/* int main()
{
	if ( ev3_init() == -1 ) return ( 1 );
  
 	while ( ev3_sensor_init() < 1 ) Sleep( 1000 );
  
	float x = 1.0; // arbitrary scale for the abscise 

	uint8_t sn_sonar;
	int portSensor = 0;
	char portNameSensor[256];
	char mode[256];
	float value = 0.0; 

	FILE* file = NULL;
	file = fopen("sonarPlot.txt", "w+"); // "a" = append "+" create it if it does not exist 

	if (ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0))
	{
		printf("Sonar found\n");

		portSensor = ev3_sensor_desc_port(sn_sonar);
		ev3_sensor_port_name(sn_sonar,portNameSensor);
		get_sensor_mode(sn_sonar,mode,sizeof(mode));

		printf("\tPort = %d %s \n",portSensor,portNameSensor);
		printf("\tmode = %s\n", mode); 


		if (file != NULL)
		{	
			float xpos=0; // starting position of the robot (relative) 
			int nbPoints = 2000;
			while (xpos < nbPoints) 
			{
				//Sleep(3); // ms
				if ( !get_sensor_value0(sn_sonar, &value)) 
				{
					value = 0;
				}	
				fprintf(file,"\n%f %f",xpos,value);
				xpos = xpos+x;
				printf("%f",xpos);
				fflush( stdout );
				
			}   
		}
		else { printf("unable to open the file"); }
	}
	else { printf("Sonar NOT found \n");}
	
	printf("END");
	fclose(file);
	
	ev3_uninit();

	return ( 0 );
	
} */
 
  



